package com.android.worldwideweather.data.network

object ApiConstants {

    const val pathFindCities: String = "v1/geo/cities"
    const val citiesLimit: String = "4"

    const val pathGetWeather: String = "forecast"
    const val pathGetCurrentWeather: String = "weather"
    const val weatherApiKey: String = "f4bcc0bfdb19cf1381e4d4e504f637d1"
    const val weatherApiUnitType: String = "metric"
}
