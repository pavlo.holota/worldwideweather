package com.android.worldwideweather.presentation.ui.flow

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.android.worldwideweather.R
import com.android.worldwideweather.data.dto.response.GetWeatherResponse
import com.android.worldwideweather.data.repository.Status
import com.google.android.gms.location.LocationServices
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.layout_weather_toolbar.*
import java.util.*


class MainActivity : FragmentActivity() {

    private lateinit var navController: NavController
    private lateinit var viewModel: WeatherViewModel

    companion object {
        private const val LOCATION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = WeatherViewModel.getInstanceForActivity(this)

        setContentView(R.layout.layout_main_activity)

        navController = findNavController(R.id.weatherNavigationNavHost)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.label) {
                "HomeWeatherFragment" -> showToolbarWorld()
                "CitySelectFragment" -> showToolbarBack()
            }
        }
    }

    override fun onStart() {
        super.onStart()

        viewModel.getWeatherLiveData().value?.data?.let {
            setupToolbar(it)
        } ?: run {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                when (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )) {
                    PackageManager.PERMISSION_GRANTED -> {
                        requestPermissionsGranted()
                    }
                    PackageManager.PERMISSION_DENIED -> {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                            requestPermissionsDenied(true)
                        } else {
                            requestPermissions(
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                LOCATION_REQUEST_CODE
                            )
                        }
                    }
                }
            } else {
                requestPermissionsGranted()
            }
        }
    }

    private fun setupToolbar(weatherResponse: GetWeatherResponse?) {
        toolbarTitle.text = weatherResponse?.city?.name ?: getString(R.string.toolbar_title_error)

        toolbarWorldIcon.setOnClickListener { navController.navigate(R.id.action_homeWeatherFragment_to_citySelectFragment) }
        toolbarBackIcon.setOnClickListener { navController.navigateUp() }
    }

    private fun showToolbarBack() {
        toolbarBackIcon.visibility = View.VISIBLE
        toolbarWorldIcon.visibility = View.GONE
    }

    private fun showToolbarWorld() {
        toolbarWorldIcon.visibility = View.VISIBLE
        toolbarBackIcon.visibility = View.GONE
    }

    private fun onApiError() {
        toolbarTitle.text = getString(R.string.toolbar_title_error)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode == LOCATION_REQUEST_CODE && (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED)
        ) {
            requestPermissionsGranted()
        } else {
            val allowRetry =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) shouldShowRequestPermissionRationale(
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) else false
            requestPermissionsDenied(allowRetry)
        }
    }

    private fun requestPermissionsDenied(askAgain: Boolean) {
        val alertDialog = MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.location_denied_title))
            .setMessage(getString(if (askAgain) R.string.location_denied_message_warning else R.string.location_denied_message))
            .setNeutralButton(getString(R.string.location_denied_button_ok)) { _, _ ->
                setupToolbar(null)
                navigateToHome()
            }

        if (askAgain && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) alertDialog.setPositiveButton(
            getString(R.string.location_denied_button_retry)
        ) { _, _ ->
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_REQUEST_CODE
            )
        }

        alertDialog.show()
    }

    private fun requestPermissionsGranted() {
        LocationServices.getFusedLocationProviderClient(this).lastLocation.addOnSuccessListener {
            it?.let {
                val addresses: List<Address> =
                    Geocoder(this, Locale.getDefault()).getFromLocation(
                        it.latitude,
                        it.longitude,
                        1
                    )
                val city: String = addresses[0].locality
                val countryCode: String = addresses[0].countryCode

                viewModel.fetchWeather(city, countryCode)
            } ?: run {
                setupToolbar(null)
            }

            navigateToHome()
        }
    }

    private fun navigateToHome() {
        navController.navigate(R.id.action_splashFragment_to_homeWeatherFragment)

        viewModel.getWeatherLiveData().observe(this, Observer { resource ->
            when (resource?.status) {
                Status.SUCCESS -> {
                    resource.data?.let {
                        setupToolbar(it)
                    } ?: onApiError()
                }
                Status.ERROR -> {
                    onApiError()
                }
                Status.LOADING -> {
                }
            }
        })
    }
}